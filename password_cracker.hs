import Data.Char (chr, ord)
import System.IO

-- password length -> starting ASCII value -> number of chars in charset -> list of all combinations
bruteCombos :: Int -> Int -> Int -> [String]
bruteCombos 0 _ _ = [[]]
bruteCombos l s n  = concat [map (chr x:) (bruteCombos (l-1) s n) | x <- [s..(s + n)]]

--writeStrings :: Handle -> [String]

main = do
    putStrLn "Enter filename: "
    name <- getLine
    file <- openFile name WriteMode

    putStrLn "Enter max password length: "
    len <- readLn :: IO Int

    putStrLn "Enter starting character: "
    start <- readLn :: IO Char

    putStrLn "Enter number of chars in charset: "
    n <- readLn :: IO Int

    mapM_ (hPutStrLn file) (bruteCombos len (ord start) n)
    hClose file
